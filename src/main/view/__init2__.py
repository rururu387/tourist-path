# import math, sys
# from PyQt6 import QtWidgets, QtCore, QtGui
# from PyQt6.QtCore import QPoint, QPointF
# from PyQt6.QtGui import QColor, QBrush
#
#
# class Path(QtWidgets.QGraphicsPathItem):
#     def __init__(self, source: QtCore.QPointF = None, destination: QtCore.QPointF = None, *args, **kwargs):
#         super(Path, self).__init__(*args, **kwargs)
#
#         self._sourcePoint = source
#         self._destinationPoint = destination
#
#         self._arrow_height = 5
#         self._arrow_width = 4
#
#     def setSource(self, point: QtCore.QPointF):
#         self._sourcePoint = point
#
#     def setDestination(self, point: QtCore.QPointF):
#         self._destinationPoint = point
#
#     def directPath(self):
#         path = QtGui.QPainterPath(self._sourcePoint)
#         path.lineTo(self._destinationPoint)
#         return path
#
#     def arrowCalc(self, start_point=None, end_point=None):  # calculates the point where the arrow should be drawn
#
#         try:
#             startPoint, endPoint = start_point, end_point
#
#             if start_point is None:
#                 startPoint = self._sourcePoint
#
#             if endPoint is None:
#                 endPoint = self._destinationPoint
#
#             dx, dy = startPoint.x() - endPoint.x(), startPoint.y() - endPoint.y()
#
#             leng = math.sqrt(dx ** 2 + dy ** 2)
#             normX, normY = dx / leng, dy / leng  # normalize
#
#             # perpendicular vector
#             perpX = -normY
#             perpY = normX
#
#             leftX = endPoint.x() + self._arrow_height * normX + self._arrow_width * perpX
#             leftY = endPoint.y() + self._arrow_height * normY + self._arrow_width * perpY
#
#             rightX = endPoint.x() + self._arrow_height * normX - self._arrow_width * perpX
#             rightY = endPoint.y() + self._arrow_height * normY - self._arrow_width * perpY
#
#             point2 = QtCore.QPointF(leftX, leftY)
#             point3 = QtCore.QPointF(rightX, rightY)
#
#             return QtGui.QPolygonF([point2, endPoint, point3])
#
#         except (ZeroDivisionError, Exception):
#             return None
#
#     def paint(self, painter: QtGui.QPainter, option, widget=None) -> None:
#
#         # painter.setRenderHint(painter.Antialiasing)
#
#         # painter.pen().setWidth(2)
#         # painter.pen().setColor(QColor().blue())
#         brush = QBrush()
#         brush.setColor(QColor().blue())
#         painter.setBrush(brush)
#         # painter.setBrush(QtCore.Qt.BrushStyle.NoBrush)
#
#         path = self.directPath()
#         painter.drawPath(path)
#         self.setPath(path)
#
#         triangle_source = self.arrowCalc(path.pointAtPercent(0.1), self._sourcePoint)  # change path.PointAtPercent() value to move arrow on the line
#
#         if triangle_source is not None:
#             painter.drawPolyline(triangle_source)
#
#
# class ViewPort(QtWidgets.QGraphicsView):
#
#     def __init__(self):
#         super(ViewPort, self).__init__()
#
#
# def main():
#     app = QtWidgets.QApplication(sys.argv)
#
#     window = ViewPort()
#     scene = QtWidgets.QGraphicsScene()
#     window.setScene(scene)
#     scene.addItem(Path(QPointF(0, 0), QPointF(100, 100)))
#
#     window.show()
#
#     sys.exit(app.exec())
#
#
# if __name__ == "__main__":
#     main()