import math

from PyQt6 import QtCore, QtWidgets
from PyQt6.QtCore import QRectF, Qt
from PyQt6.QtGui import QColor, QFont, QPen, QPainter, QPainterPath, QPainterPathStroker
from PyQt6.QtSvgWidgets import QGraphicsSvgItem
from PyQt6.QtWidgets import QGraphicsView, QGraphicsScene, QGraphicsTextItem, QMessageBox

from initializer.TestRandomGraphFactory import TestRandomGraphFactory
from model.data.Vertex import Vertex
from model.service.GraphPathService import GraphPathService
from view.Path import Path
from view.graphicsItem.EdgeGraphicsRepresentation import EdgeGraphicsRepresentation
from view.graphicsItem.VertexGraphicsRepresentation import VertexGraphicsRepresentation


class MapViewer(QGraphicsView):
    vertexes: list[Vertex]
    vertexGraphicsItems: dict[int, VertexGraphicsRepresentation]
    edges: list[dict[int, int]]
    edgeGraphicsItems: list[dict[int, EdgeGraphicsRepresentation]]
    mapClicked = QtCore.pyqtSignal(QtCore.QPointF)
    strokedEdges: list[EdgeGraphicsRepresentation]
    textItem: QGraphicsTextItem | None

    def __init__(self, parent):
        super(MapViewer, self).__init__(parent)
        self.scene = QGraphicsScene()
        self.setRenderHint(QPainter.RenderHint.Antialiasing)
        self.textItem = None
        self.setScene(self.scene)
        self.initGraph()
        self.isBeingMoved = False
        self.setTransformationAnchor(QGraphicsView.ViewportAnchor.AnchorUnderMouse)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.strokedEdges = list()

    def initGraph(self) -> None:
        self.vertexes = TestRandomGraphFactory.generateVertexes(20, 0, 2000, 0, 2000)
        self.edges = TestRandomGraphFactory.generateEdges(self.vertexes)

    def draw(self, sceneRect: QRectF):
        minSize: float = min(self.width() - 2, self.height() - 2)
        self.scene.clear()
        self.showVertexes(2000 / minSize)

        self.showEdges(2000 / minSize)
        self.scene.setSceneRect(sceneRect)
        self.scale(minSize / 2000, minSize / 2000)

        self.scale(1.3, 1.3)

    def showVertexes(self, scale: float):
        self.vertexSvgs = dict()
        scale *= 0.2

        for vertex in self.vertexes:
            imageItem: QGraphicsSvgItem = QGraphicsSvgItem("resources/icons/source/map-pin-icon.svg")
            imageItem.setScale(scale)
            imageItem.setX(vertex.x() - imageItem.sceneBoundingRect().width() / 2)
            imageItem.setY(vertex.y() - imageItem.sceneBoundingRect().height())
            self.scene.addItem(imageItem)
            font: QFont = QFont()
            font.setPixelSize(40)
            textItem: QGraphicsTextItem = self.scene.addText(str(vertex.num), font)
            textItem.setDefaultTextColor(QColor.fromRgb(0, 0, 0))
            textItem.setX(imageItem.sceneBoundingRect().x() + ((imageItem.sceneBoundingRect().width() - textItem.sceneBoundingRect().width()) / 2))
            textItem.setY(imageItem.sceneBoundingRect().y() - textItem.sceneBoundingRect().height())

            self.vertexSvgs[vertex.num] = VertexGraphicsRepresentation(imageItem, textItem)

    def getEdgeColor(self, edges: list[dict[int, int]], sourceNum: int, destNum: int):
        sourceVertex = self.vertexes[sourceNum]
        destinationVertex = self.vertexes[destNum]

        distance: float = sourceVertex.getDistance(destinationVertex)
        weight: int = edges[sourceNum][destNum]
        distanceRatio = distance / weight
        if distanceRatio >= TestRandomGraphFactory.maxTrafficCoefficient:
            return QColor().fromRgb(255, 0, 30)
        if distanceRatio <= TestRandomGraphFactory.minTrafficCoefficient:
            return QColor().fromRgb(0, 255, 30)
        return QColor().fromRgb(int((distanceRatio - 0.5) * 255), int((1.5 - distanceRatio) * 255), 60)

    def reDrawEdges(self):
        for vertexEdgeMap in self.edgeGraphicsItems:
            for destVertexNum, edgeGraphicsRepresentation in vertexEdgeMap.items():
                edgeGraphicsRepresentation.erase(self.scene)
        minSize: float = min(self.width() - 2, self.height() - 2)
        self.showEdges(2000 / minSize)

    def showEdges(self, scale: float):
        self.edgeGraphicsItems = list()
        for vertex in self.vertexes:
            self.edgeGraphicsItems.append(dict())
            for destination, weight in self.edges[vertex.num].items():
                edgeColor: QColor = self.getEdgeColor(self.edges, vertex.num, destination)
                path: Path = Path(vertex.toPointF(), self.vertexes[destination].toPointF(), scale, edgeColor)
                path.draw(self.scene)
                # self.scene.addItem(path)
                self.edgeGraphicsItems[vertex.num][destination] = path.graphicsRepresentation

    def showPath(self, pathNodes: list[int]):
        if pathNodes is None or len(pathNodes) < 1:
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Icon.Warning)
            msgBox.setWindowTitle("Ошибка при выполнении программы")
            msgBox.setText("Подходящего маршрута не существует. Попробуйте задать больший лимит по времени")
            msgBox.exec()
            return
        if len(pathNodes) == 1:
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Icon.Information)
            msgBox.setText("Не удалось найти маршрут, содержащий достопримечательности помимо той, что "
                           "находится в точке отправления. Попробуйте задать больший лимит по времени")
            msgBox.exec()
            return

        for strokedEdge in self.strokedEdges:
            strokedEdge.stroke(self.scene, EdgeGraphicsRepresentation.defaultThickness)

        self.strokedEdges.clear()

        sourceVertexNum: int = pathNodes[0]
        for curEdgeIdx in range(1, len(pathNodes)):
            destinationVertexNum: int = pathNodes[curEdgeIdx]
            edgeGraphics: EdgeGraphicsRepresentation = self.edgeGraphicsItems[sourceVertexNum][destinationVertexNum]
            edgeGraphics.stroke(self.scene)
            self.strokedEdges.append(edgeGraphics)
            sourceVertexNum = destinationVertexNum

        actualTime: int = GraphPathService.getPathWeight(self.edges, pathNodes)
        seconds: int = actualTime % 60
        actualTime = int(actualTime / 60)
        minutes: int = actualTime % 60
        hours = int(actualTime / 60)
        text: str = ""
        if hours > 0:
            text += str(hours) + "ч "
        if minutes > 0:
            text += str(minutes) + "м "
        if seconds > 0:
            text += str(seconds) + "с"
        font: QFont = QFont()
        font.setPixelSize(30)
        if self.textItem is not None:
            self.scene.removeItem(self.textItem)
        self.textItem: QGraphicsTextItem = self.scene.addText(text + " на дорогу", font)
        self.textItem.setDefaultTextColor(QColor.fromRgb(0, 0, 0))
        self.textItem.setX(self.vertexes[pathNodes[-1]].x() - self.textItem.sceneBoundingRect().width() / 2)
        self.textItem.setY(self.vertexes[pathNodes[-1]].y())

    def showEvent(self, event):
        self.draw(QRectF(0, 0, 2000, 2000))

    def wheelEvent(self, event):
        angle = event.angleDelta().y()
        if angle > 0:
            factor = 1.1
        else:
            factor = 0.9
        self.scale(factor, factor)

    def mouseReleaseEvent(self, event):
        self.setDragMode(QtWidgets.QGraphicsView.DragMode.NoDrag)

    def mousePressEvent(self, event):
        self.mapClicked.emit(self.mapToScene(event.position().toPoint()))
        self.setDragMode(QtWidgets.QGraphicsView.DragMode.ScrollHandDrag)
        super(MapViewer, self).mousePressEvent(event)
