#!/bin/bash
scriptDir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
(
cd "$scriptDir" || exit
pyuic6 ../../resources/window/MainWindow.ui -o Ui_MainWindow.py
)