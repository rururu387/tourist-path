from PyQt6.QtCore import QLineF
from PyQt6.QtGui import QPainterPath, QPen
from PyQt6.QtWidgets import QGraphicsScene, QGraphicsPathItem, QGraphicsLineItem


class EdgeGraphicsRepresentation:
    path: QPainterPath
    arrow: (QLineF, QLineF)
    pathItem: QGraphicsPathItem
    arrowItem: (QGraphicsLineItem, QGraphicsLineItem)
    pen: QPen
    defaultThickness: int = 4

    def __init__(self, path: QPainterPath, arrow: (QLineF, QLineF)):
        self.path = path
        self.arrow = arrow

    def draw(self, scene: QGraphicsScene, pen: QPen = None):
        if pen is None:
            pen = self.pen

        self.pen = pen
        self.pathItem = scene.addPath(self.path, pen)

        if self.arrow is not None:
            self.arrowItem = scene.addLine(self.arrow[0], pen), scene.addLine(self.arrow[1], pen)

    def erase(self, scene: QGraphicsScene):
        scene.removeItem(self.pathItem)
        if self.arrow is not None:
            scene.removeItem(self.arrowItem[0])
            scene.removeItem(self.arrowItem[1])

    def stroke(self, scene: QGraphicsScene, thickness: int = None):
        if thickness is None:
            thickness = 3 * EdgeGraphicsRepresentation.defaultThickness
        self.erase(scene)
        self.pen.setWidth(thickness)
        self.draw(scene)

