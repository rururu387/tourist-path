from PyQt6.QtSvgWidgets import QGraphicsSvgItem
from PyQt6.QtWidgets import QGraphicsTextItem


class VertexGraphicsRepresentation:
    image: QGraphicsSvgItem
    text: QGraphicsTextItem

    def __init__(self, pinImage: QGraphicsSvgItem, text: QGraphicsTextItem):
        self.image = pinImage
        self.text = text
