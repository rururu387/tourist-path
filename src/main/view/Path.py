import math

from PyQt6 import QtWidgets, QtCore, QtGui
from PyQt6.QtCore import QPointF, QLineF
from PyQt6.QtGui import QColor, QPen
from PyQt6.QtWidgets import QGraphicsScene

from view.graphicsItem.EdgeGraphicsRepresentation import EdgeGraphicsRepresentation


class Path(QtWidgets.QGraphicsPathItem):
    color: QColor
    angle: float
    offsetAngle: float
    _sourcePoint: QPointF
    _destinationPoint: QPointF
    graphicsRepresentation: EdgeGraphicsRepresentation

    def __init__(self, source: QtCore.QPointF, destination: QtCore.QPointF, scale: float, color: QColor, *args,
                 **kwargs):
        super(Path, self).__init__(*args, **kwargs)
        self.offsetAngle = math.pi / 6
        self.__initAngle__(source, destination)
        self.setSourcePoint(source)
        self.setDestinationPoint(destination)

        self._arrow_height = 4 * scale
        self._arrow_width = 3 * scale
        self.color = color

    def __initAngle__(self, source: QPointF, destination: QPointF):
        if destination.x() == source.x():
            if destination.y() > source.y():
                self.angle = math.pi / 2
            else:
                self.angle = 3 * math.pi / 2
        else:
            self.angle = math.atan((destination.y() - source.y()) / (destination.x() - source.x()))
        if source.x() > destination.x():
            self.angle += math.pi

    def setSourcePoint(self, point: QtCore.QPointF):
        radialOffset: float = 10

        # Offset - смещение пути, чтобы встречные дороги не накладывались друг на друга
        offsetX = radialOffset * math.cos(self.angle + self.offsetAngle)
        offsetY = radialOffset * math.sin(self.angle + self.offsetAngle)

        self._sourcePoint = QPointF(point.x() + offsetX, point.y() + offsetY)

    def setDestinationPoint(self, point: QtCore.QPointF):
        radialOffset: float = 10

        # Offset - смещение пути, чтобы встречные дороги не накладывались друг на друга
        offsetX = radialOffset * math.cos(math.pi + self.angle - self.offsetAngle)
        offsetY = radialOffset * math.sin(math.pi + self.angle - self.offsetAngle)

        self._destinationPoint = QPointF(point.x() + offsetX, point.y() + offsetY)

    def directPath(self):
        path = QtGui.QPainterPath(QPointF(self._sourcePoint))
        path.lineTo(QPointF(self._destinationPoint))
        return path

    def arrowCalc(self, startPoint: QPointF = None, endPoint: QPointF = None) -> (QLineF, QLineF):
        if startPoint is None:
            startPoint = self._sourcePoint

        if endPoint is None:
            endPoint = self._destinationPoint

        dx, dy = startPoint.x() - endPoint.x(), startPoint.y() - endPoint.y()

        leng = math.sqrt(dx ** 2 + dy ** 2)
        normX, normY = dx / leng, dy / leng  # normalize

        # perpendicular vector
        perpX = -normY
        perpY = normX

        leftX = endPoint.x() + self._arrow_height * normX + self._arrow_width * perpX
        leftY = endPoint.y() + self._arrow_height * normY + self._arrow_width * perpY

        rightX = endPoint.x() + self._arrow_height * normX - self._arrow_width * perpX
        rightY = endPoint.y() + self._arrow_height * normY - self._arrow_width * perpY

        point2 = QPointF(leftX, leftY)
        point3 = QPointF(rightX, rightY)

        return QLineF(point2, endPoint), QLineF(point3, endPoint)

    def draw(self, scene: QGraphicsScene) -> None:
        path = self.directPath()
        self.setPath(path)
        arrow: (QLineF, QLineF) = self.arrowCalc(self._sourcePoint, self._destinationPoint)
        self.graphicsRepresentation = EdgeGraphicsRepresentation(path, arrow)

        pen: QPen = QPen()
        pen.setWidth(EdgeGraphicsRepresentation.defaultThickness)
        pen.setColor(self.color)

        self.graphicsRepresentation.draw(scene, pen)
