import math

from PyQt6.QtCore import QPointF, QTime
from PyQt6.QtWidgets import QMainWindow, QMessageBox, QComboBox, QGraphicsTextItem

from model.data.Vertex import Vertex
from model.service.search.PathSearchService import PathSearchService
from model.service.search.complete.ModifiedCompleteSearchService import ModifiedCompleteSearchService
from model.service.search.dijkstra.ModifiedUniformCostSearchService import ModifiedUniformCostSearchService
from model.service.search.dijkstra.UniformCostSearchService import UniformCostSearchService
from view.generate.Ui_MainWindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    startPoint: Vertex | None
    endPoint: Vertex | None

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        self.startPoint = None
        self.endPoint = None
        self.startBtn.clicked.connect(self.pickStartPoint)
        self.endBtn.clicked.connect(self.pickEndPoint)
        self.buildBtn.clicked.connect(self.drawPath)

    def pickStartPoint(self):
        self.pickPoint(self.setStartPoint)

    def setStartPoint(self, event):
        self.mapView.mapClicked.disconnect()
        vertex: Vertex = self.getVertexByEvent(event)
        if vertex is not None:
            self.startPoint = vertex
            self.startBtn.setText(vertex.name)

    def pickEndPoint(self):
        self.pickPoint(self.setEndPoint)

    def setEndPoint(self, event):
        self.mapView.mapClicked.disconnect()
        vertex: Vertex = self.getVertexByEvent(event)
        if vertex is not None:
            self.endPoint = vertex
            self.endBtn.setText(vertex.name)

    def getVertexByEvent(self, clickPoint: QPointF) -> Vertex:
        closestVertex: Vertex | None = None
        minDistance: float | None = None
        for vertex in self.mapView.vertexes:
            distance: float = math.sqrt(((vertex.x() - clickPoint.x()) ** 2) + ((vertex.y() - clickPoint.y()) ** 2))
            if (minDistance is None or distance < minDistance) and self.mapView.vertexSvgs[vertex.num].image.sceneBoundingRect().contains(clickPoint):
                closestVertex = vertex
                minDistance = distance
        return closestVertex

    def pickPoint(self, consumer):
        self.mapView.mapClicked.connect(consumer)

    def drawPath(self):
        if self.startPoint is None or self.endPoint is None:
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Icon.Information)
            msgBox.setText("Точки начала и конца маршрута не выбраны")
            msgBox.exec()
            return

        pickedTime: QTime = self.timePicker.time()

        if pickedTime.hour() == 0 and pickedTime.minute() == 0:
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Icon.Information)
            msgBox.setText("Лимит времени в пути не задан")
            msgBox.exec()
            return

        surmountableDistance: int = (pickedTime.hour() * 60 + pickedTime.minute()) * 60
        pickedAlgorithm: QComboBox = self.algorithmPicker
        pathSearchService: PathSearchService
        match pickedAlgorithm.currentText():
            case "Туристический путь":
                pathSearchService = ModifiedUniformCostSearchService()
            case "Кратчайший путь":
                pathSearchService = UniformCostSearchService()
            case "Полный перебор (туристический путь)":
                pathSearchService = ModifiedCompleteSearchService()
            case _:
                msgBox = QMessageBox()
                msgBox.setIcon(QMessageBox.Icon.Warning)
                msgBox.setWindowTitle("Ошибка при выполнении программы")
                msgBox.setText("Не найден выбранный алгоритм: " + str(pickedAlgorithm))
                msgBox.exec()
                return

        pathNodes: list[int] = pathSearchService.getPath(self.mapView.edges, self.startPoint.num, self.endPoint.num, surmountableDistance)
        self.mapView.showPath(pathNodes)
