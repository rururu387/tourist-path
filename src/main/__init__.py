import sys
from typing import Any

from PyQt6.QtWidgets import QApplication

from view.MainWindow import MainWindow


class Application(QApplication):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super(Application, self).__init__(*args, **kwargs)
        self.setApplicationName("Tourist path builder")


def main():
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    app.exec()


if __name__ == "__main__":
    main()
