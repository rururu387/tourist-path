class VertexWithWeight:
    vertex: int
    weight: int

    def __init__(self, vertex: int, weight: int):
        self.vertex = vertex
        self.weight = weight