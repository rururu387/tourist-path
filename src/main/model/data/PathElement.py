from model.data.Edge import Edge


class PathElement:
    sourceVertex: int
    destinationVertex: int
    weight: int
    destinationWeight: int
    edgeCount: int

    def __init__(self, sourceVertex: int, destinationVertex: int, weight: int, destinationWeight: int, edgeCount: int):
        self.sourceVertex = sourceVertex
        self.destinationVertex = destinationVertex
        self.weight = weight
        self.destinationWeight = destinationWeight
        self.edgeCount = edgeCount

    def getSourceWeight(self) -> int:
        return self.weight + self.destinationWeight

    def toEdge(self) -> Edge:
        return Edge(self.sourceVertex, self.destinationWeight)

    def __le__(self, other):
        if self.destinationWeight <= other.destinationWeight:
            if self.destinationWeight == other.destinationWeight and self.edgeCount < other.edgeCount:
                return False
            return True
        return False

    def __lt__(self, other):
        if self.destinationWeight <= other.destinationWeight:
            if self.destinationWeight == other.destinationWeight and self.edgeCount <= other.edgeCount:
                return False
            return True
        return False
