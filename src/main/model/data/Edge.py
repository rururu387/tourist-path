class Edge:
    sourceVertex: int
    weight: int

    def __init__(self, sourceVertex: int, weight: int):
        self.sourceVertex = sourceVertex
        self.weight = weight
