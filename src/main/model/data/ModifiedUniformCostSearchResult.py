from model.data.ImproveElement import ImproveElement
from model.data.PathElement import PathElement


class ModifiedUniformCostSearchResult:
    vertexProperties: dict[int, PathElement]
    improveInfo: list[ImproveElement]  # Use with heapq

    def __init__(self, vertexProperties: dict[int, PathElement], improveInfo: list[ImproveElement]):
        self.vertexProperties = vertexProperties
        self.improveInfo = improveInfo
