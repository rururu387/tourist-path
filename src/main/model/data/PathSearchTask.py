class PathSearchTask:
    graph: list[dict[int, int]]
    startPoint: int
    endPoint: int
    timeSecondsThreshold: int

    def __init__(self, graph: list[dict[int, int]], startPoint: int, endPoint: int, timeSecondsThreshold: int):
        self.graph = graph
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.timeSecondsThreshold = timeSecondsThreshold