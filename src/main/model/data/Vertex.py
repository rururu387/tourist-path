from __future__ import annotations

import math

from PyQt6.QtCore import QPoint


class Vertex(QPoint):
    name: str
    num: int

    def __init__(self, x: int, y: int, num: int):
        super().__init__(x, y)
        self.num = num
        self.name = "Точка " + str(num)

    def getDistance(self, other: Vertex) -> float:
        return math.sqrt((self.x() - other.x()) ** 2 + (self.y() - other.y()) ** 2)

    def __lt__(self, other):
        return self.num < other.num
