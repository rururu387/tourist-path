from model.data.PathElement import PathElement


class ImproveElement(PathElement):
    edgeDifference: int
    lengthDifference: int

    def __init__(self, current: PathElement, suggested: PathElement):
        super().__init__(suggested.sourceVertex, suggested.destinationVertex, suggested.weight, suggested.destinationWeight, suggested.edgeCount)
        self.edgeDifference = suggested.edgeCount - current.edgeCount
        self.lengthDifference = suggested.destinationWeight - current.destinationWeight

    def __lt__(self, other) -> bool:
        if self.edgeDifference >= other.edgeDifference:
            if self.edgeDifference == other.edgeDifference and self.lengthDifference >= other.lengthDifference:
                return False
            return True
        return False
