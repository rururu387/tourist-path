from model.data.PathElement import PathElement


class GraphPathService:
    @staticmethod
    def getShortestPath(vertexes: dict[int, PathElement], startVertex: int, destinationVertex: int) -> list[int] | None:
        if startVertex == destinationVertex:
            return []
        graphPath = list()
        while destinationVertex != startVertex:
            vertexProperties: PathElement = vertexes.get(destinationVertex)
            if vertexProperties is None:
                return None
            graphPath.append(destinationVertex)
            destinationVertex = vertexProperties.sourceVertex
        graphPath.append(startVertex)
        graphPath.reverse()
        return graphPath

    @staticmethod
    def getPathWeight(graph: list[dict[int, int]], path: list[int]) -> int:
        if len(path) == 0:
            return 0
        weight = 0
        edgeStartVertex = path[0]
        for i in range(1, len(path)):
            edgeEndVertex = path[i]
            weight += graph[edgeStartVertex][edgeEndVertex]
            edgeStartVertex = edgeEndVertex
        return weight
