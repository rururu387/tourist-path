from model.data.PathSearchTask import PathSearchTask


class PathSearchService:
    def getPathByTask(self, pathSearchTask: PathSearchTask) -> list[int]:
        return self.getPath(pathSearchTask.graph, pathSearchTask.startPoint, pathSearchTask.endPoint,
                            pathSearchTask.timeSecondsThreshold)

    def getPath(self, graph: list[dict[int, int]], startPoint: int, endPoint: int, timeSecondsThreshold: int) -> list[int] | None:
        pass
