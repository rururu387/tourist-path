import heapq

import numpy as np

from model.data.ModifiedUniformCostSearchResult import ModifiedUniformCostSearchResult
from model.data.ImproveElement import ImproveElement
from model.data.PathElement import PathElement
from model.service.GraphPathService import GraphPathService
from model.service.PathAnalyserService import PathAnalyser
from model.service.search.PathSearchService import PathSearchService


class ModifiedUniformCostSearchService(PathSearchService):

    def getPath(self, graph: list[dict[int, int]], startPoint: int, destinationPoint: int, timeSecondsThreshold: int) -> list[int]:
        modifiedResult = ModifiedUniformCostSearchService.__modifiedUniformCostSearch__(graph, startPoint, timeSecondsThreshold)
        return ModifiedUniformCostSearchService.getImprovedPath(graph, modifiedResult, startPoint, destinationPoint, timeSecondsThreshold)

    @staticmethod
    def getModifiedPath(vertexes: dict[int, list[PathElement]], startVertex: int, destinationVertex: int) -> list[int] | None:
        if startVertex == destinationVertex:
            return []
        pathElementMap = dict()
        for vertexNum, pathElementList in vertexes.items():
            pathElementMap[vertexNum] = len(pathElementList) - 1
        graphPath = list()
        while destinationVertex != startVertex:
            pathElementCounter: int = pathElementMap.get(destinationVertex)
            if pathElementCounter is None:
                return None
            vertexProperties: PathElement = vertexes.get(destinationVertex)[pathElementCounter]
            if pathElementCounter > 0:
                pathElementMap[destinationVertex] = pathElementCounter - 1
            if vertexProperties is None:
                return None
            graphPath.append(destinationVertex)
            destinationVertex = vertexProperties.sourceVertex
        graphPath.append(startVertex)
        graphPath.reverse()
        return graphPath

    @staticmethod
    def getPathWeight(graph: list[dict[int, int]], path: list[int]) -> int:
        if len(path) == 0:
            return 0
        weight = 0
        edgeStartVertex = path[0]
        for i in range(1, len(path)):
            edgeEndVertex = path[i]
            weight += graph[edgeStartVertex][edgeEndVertex]
            edgeStartVertex = edgeEndVertex
        return weight

    @staticmethod
    def __modifiedUniformCostSearch__(graph: list[dict[int, int]], startPoint: int, lengthThreshold: int) \
            -> ModifiedUniformCostSearchResult:
        queue: list[PathElement] = []
        improveInfo: list[ImproveElement] = []
        visitedVertexes: dict[int, PathElement | None] = dict()
        heapq.heappush(queue, PathElement(startPoint, startPoint, 0, 0, 0))
        visitedVertexes[startPoint] = None

        while len(queue) != 0:
            curEdge: PathElement = heapq.heappop(queue)

            oldEdge: PathElement = visitedVertexes.get(curEdge.destinationVertex)
            if oldEdge is not None:
                if curEdge.edgeCount > oldEdge.edgeCount:
                    heapq.heappush(improveInfo, ImproveElement(oldEdge, curEdge))
                continue

            assignedWeight: int | None
            pathElement: PathElement
            sourceVertexProps: PathElement | None = visitedVertexes.get(curEdge.sourceVertex)
            if sourceVertexProps is not None:
                edgeLen = graph[curEdge.sourceVertex].get(curEdge.destinationVertex)
                assignedWeight = sourceVertexProps.destinationWeight + edgeLen
                pathElement = curEdge
            elif curEdge.sourceVertex in visitedVertexes.keys():
                assignedWeight = 0
                pathElement = PathElement(curEdge.sourceVertex, curEdge.sourceVertex, 0, 0, 0)
            else:
                raise Exception("Ошибка в реализации алгоритма")
            visitedVertexes[curEdge.destinationVertex] = pathElement
            if assignedWeight > lengthThreshold:
                continue
            outgoingEdges: dict[int, int] = graph[curEdge.destinationVertex]
            for edgeSourceVertex, edgeWeight in outgoingEdges.items():
                heapq.heappush(queue,
                               PathElement(curEdge.destinationVertex, edgeSourceVertex, edgeWeight, assignedWeight +
                                           edgeWeight, curEdge.edgeCount + 1))
        return ModifiedUniformCostSearchResult(visitedVertexes, improveInfo)

    @staticmethod
    def getImprovedPath(graph: list[dict[int, int]], modifiedResult: ModifiedUniformCostSearchResult,
                        startVertex: int,
                        destinationVertex: int, lengthThreshold: int) -> list[int] | None:
        if startVertex == destinationVertex:
            return []
        improveVertexProperties: dict[int, list[PathElement]] = dict()
        for i in range(0, len(graph)):
            vertexProperty = modifiedResult.vertexProperties.get(i)
            if vertexProperty is None:
                continue
            vertexPropList = list()
            vertexPropList.append(vertexProperty)
            improveVertexProperties[i] = vertexPropList
        improveInfo = modifiedResult.improveInfo
        graphPath: list[int] = ModifiedUniformCostSearchService.getModifiedPath(improveVertexProperties, startVertex, destinationVertex)
        if graphPath is None:
            return None
        lengthDifference: int = lengthThreshold - GraphPathService.getPathWeight(graph, graphPath)

        if lengthDifference < 0:
            return None

        while len(improveInfo) > 0:
            element: ImproveElement = heapq.heappop(improveInfo)
            if len(improveVertexProperties[element.destinationVertex]) == 0:
                continue
            pathToVertex = improveVertexProperties[element.destinationVertex][0]
            if pathToVertex.edgeCount >= element.edgeCount or element.lengthDifference > lengthDifference or element.destinationVertex not in graphPath:
                continue

            newGraphPath: list[int] = []
            improveVertexProperties[element.destinationVertex].append(element)
            pathStart: list[int] = ModifiedUniformCostSearchService.getModifiedPath(improveVertexProperties, startVertex,
                                                                                    element.destinationVertex)

            if pathStart is not None:
                newGraphPath = pathStart
            i: int = 0
            while i < len(graphPath) and graphPath[i] != element.destinationVertex:
                i = i + 1
            i = i + 1
            j = i + 1
            while i < len(graphPath):
                newGraphPath.append(graphPath[i])
                i = i + 1
            if len(np.unique(newGraphPath)) > len(np.unique(graphPath)) and PathAnalyser.getLength(graph, newGraphPath) <= lengthThreshold:
                isCycle: bool = False
                for i in range(0, len(pathStart) - 2):
                    if element.destinationVertex == pathStart[i]:
                        isCycle = True
                        break

                if not isCycle:
                    improveVertexProperties[element.destinationVertex].pop()
                    improveVertexProperties[element.destinationVertex][0] = element

                while j < len(graphPath):
                    prevProp = improveVertexProperties[graphPath[j - 1]]
                    improveVertexProperties[graphPath[j]].append(PathElement(graphPath[j - 1], graphPath[j], element.weight, prevProp[len(prevProp) - 1].destinationWeight + element.weight, prevProp[len(prevProp) - 1].edgeCount + 1))
                    j = j + 1
                graphPath = newGraphPath
            else:
                improveVertexProperties[element.destinationVertex].pop()

            lengthDifference -= element.lengthDifference

        return graphPath
