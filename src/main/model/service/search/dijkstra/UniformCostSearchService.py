import heapq

from model.data.PathElement import PathElement
from model.service.GraphPathService import GraphPathService
from model.service.search.PathSearchService import PathSearchService


class UniformCostSearchService(PathSearchService):

    def getPath(self, graph: list[dict[int, int]], startPoint: int, destinationPoint: int, timeSecondsThreshold: int) -> list[int] | None:
        vertexData = UniformCostSearchService.__uniformCostSearch__(graph, startPoint, timeSecondsThreshold)
        return GraphPathService.getShortestPath(vertexData, startPoint, destinationPoint)

    @staticmethod
    def __uniformCostSearch__(graph: list[dict[int, int]], startPoint: int, lengthThreshold: int) -> dict[int, PathElement]:
        queue: list[PathElement] = []
        visitedVertexes: dict[int, PathElement | None] = dict()
        heapq.heappush(queue, PathElement(startPoint, startPoint, 0, 0, 0))
        visitedVertexes[startPoint] = None

        while len(queue) != 0:
            curEdge: PathElement = heapq.heappop(queue)

            oldEdge: PathElement = visitedVertexes.get(curEdge.destinationVertex)
            if oldEdge is not None:
                if curEdge < oldEdge:
                    visitedVertexes[curEdge.destinationVertex] = curEdge
                continue

            assignedWeight: int | None
            pathElement: PathElement
            sourceVertexProps = visitedVertexes.get(curEdge.sourceVertex)
            if sourceVertexProps is not None:
                edgeLen = graph[curEdge.sourceVertex].get(curEdge.destinationVertex)
                assignedWeight = sourceVertexProps.destinationWeight + edgeLen
                pathElement = curEdge
            elif curEdge.sourceVertex in visitedVertexes.keys():
                assignedWeight = 0
                pathElement = PathElement(curEdge.sourceVertex, curEdge.sourceVertex, 0, 0, 0)
            else:
                raise Exception("Ошибка в реализации алгоритма")
            visitedVertexes[curEdge.destinationVertex] = pathElement
            if assignedWeight > lengthThreshold:
                continue
            outgoingEdges: dict[int, int] = graph[curEdge.destinationVertex]
            for edgeSourceVertex, edgeWeight in outgoingEdges.items():
                if assignedWeight + edgeWeight > lengthThreshold:
                    continue
                heapq.heappush(queue, PathElement(curEdge.destinationVertex, edgeSourceVertex, edgeWeight, assignedWeight +
                                           edgeWeight, curEdge.edgeCount + 1))

        return visitedVertexes