import operator

import numpy as np

from model.service.search.PathSearchService import PathSearchService


class ModifiedCompleteSearchService(PathSearchService):
    def getPath(self, graph: list[dict[int, int]], startPoint: int, endPoint: int, timeSecondsThreshold) -> list[int] | None:
        result: tuple[list[int], int] | None = ModifiedCompleteSearchService.__getPath__(graph, startPoint, endPoint, timeSecondsThreshold, [startPoint], 0)
        if result is None:
            return None
        return result[0]

    @staticmethod
    def rIndex(lst: list[int], value: int) -> int | None:
        rIndex: int | None = None
        for i in range(len(lst) - 1, -1, -1):
            if lst[i] == value:
                rIndex = i
                break
        return rIndex


    @staticmethod
    def concludesUselessCycle(path: list[int], vertexToAdd: int) -> bool:
        rightIndex: int | None = ModifiedCompleteSearchService.rIndex(path, vertexToAdd)
        if rightIndex is None:
            return False
        previousElements = set()
        for i in range(0, rightIndex):
             previousElements.add(path[i])

        for i in range(rightIndex + 1, len(path)):
            if path[i] not in previousElements:
                return False
        return True

    @staticmethod
    def __getPath__(graph: list[dict[int, int]], startPoint: int, endPoint: int, lengthThreshold: int,
                    curPath: list[int], curPathLen: int) -> tuple[list[int], int] | None:
        curBestPath: list[int] | None = None
        curBestPathVertexAm: int | None = None
        curBestPathLen: int | None = None
        if startPoint == endPoint:
            curBestPath = curPath
            curBestPathVertexAm = np.unique(curPath).size
            curBestPathLen = curPathLen
            # return curPath, curPathLen
        for key, value in graph[startPoint].items():
            if ModifiedCompleteSearchService.concludesUselessCycle(curPath, key):
                continue
            newPathLen: int = curPathLen + value
            if newPathLen <= lengthThreshold:
                newPath: list[int] = list(curPath)
                newPath.append(key)

                result = ModifiedCompleteSearchService.__getPath__(graph, key, endPoint, lengthThreshold, newPath,
                                                                                                            newPathLen)
                if result is None:
                    continue

                newPathComplete: list[int] = result[0]
                newPathLen: int = result[1]

                newPathVertexAm: int = np.unique(newPathComplete).size
                if (curBestPath is None or newPathVertexAm > curBestPathVertexAm or
                        (curBestPathVertexAm == newPathVertexAm and curBestPathLen > newPathLen)):
                    curBestPath = newPathComplete
                    curBestPathVertexAm = newPathVertexAm
                    curBestPathLen = newPathLen

        return curBestPath, curBestPathLen

    # @staticmethod
    # def __getPath__(graph: list[dict[int, int]], startPoint: int, endPoint: int,
    #                 lengthThreshold: int, curPath: list[int], curPathLen: int, curBestPath: CurrentBestPath) -> list[int] | None:
    #     if startPoint == endPoint:
    #         curPathVertexAm: int = np.unique(curPath).size
    #         if curPathVertexAm < curBestPath.uniqueVertexAm:
    #             return None
    #         curPathLen = PathAnalyser.getLength(graph, curPath)
    #         if (curPathVertexAm > curBestPath.uniqueVertexAm or
    #                 (curPathVertexAm == curBestPath.uniqueVertexAm and curPathLen < curBestPath.length)):
    #             curBestPath.path = curPath
    #             curBestPath.uniqueVertexAm = curPathVertexAm
    #             curBestPath.length = curPathLen
    #     for key, value in graph[startPoint].items():
    #         newPath = list(curPath)
    #         newPath.append(key)
    #         newPathLen: int = curPathLen + graph[startPoint][key]
    #         if newPathLen <= lengthThreshold:
    #             ModifiedCompleteSearchService.__getPath__(graph, key, endPoint, lengthThreshold, newPath, newPathLen, curBestPath)
    #     return curBestPath.path
