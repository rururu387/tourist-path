# import sys
#
# from service.PathAnalyserService import PathAnalyser
# from service.search.PathSearchService import PathSearchService
#
#
# class CompleteSearchService(PathSearchService):
#     bestPath: list[int] | None
#     bestVertexAm: int
#     bestLen: int
#
#     def reInit(self):
#         self.bestPath = None
#         self.bestVertexAm = 0
#         self.bestLen = sys.maxsize
#
#     @staticmethod
#     def getPath(graph: list[dict[int, int]], startPoint: int, endPoint: int, lengthThreshold) \
#                                                                                                 -> list[int] | None:
#         return CompleteSearchService.__getPath__(graph, startPoint, endPoint, lengthThreshold, [startPoint], 0)
#
#     def __getPath__(self, graph: list[dict[int, int]], startPoint: int, endPoint: int,
#                     lengthThreshold: int, curPath: list[int], curPathLen: int) -> list[int] | None:
#         if startPoint == endPoint:
#             curPathVertexAm = len(curPath)
#             if curPathVertexAm < self.bestVertexAm:
#                 return None
#             curPathLen = PathAnalyser.getLength(graph, curPath)
#             if (curPathVertexAm > self.bestVertexAm or
#                     (curPathVertexAm == self.bestVertexAm and curPathLen < self.bestLen)):
#                 self.bestPath = curPath
#                 self.bestVertexAm = curPathVertexAm
#                 self.bestLen = curPathLen
#         for key, value in graph[startPoint].items():
#             if key in curPath:
#                 continue
#             newPath = list(curPath)
#             newPath.append(key)
#             newPathLen: int = curPathLen + graph[startPoint][key]
#             if newPathLen <= lengthThreshold:
#                 self.__getPath__(graph, key, endPoint, lengthThreshold, newPath, newPathLen)
#         return self.bestPath
