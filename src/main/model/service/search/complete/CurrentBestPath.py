import sys


class CurrentBestPath:
    path: list[int] | None = None
    uniqueVertexAm: int = 0
    length: int = sys.maxsize
