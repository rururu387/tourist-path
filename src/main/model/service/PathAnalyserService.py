class PathAnalyser:
    @staticmethod
    def getLength(graph: list[dict[int, int]], path: list[int]) -> int:
        if len(path) <= 1:
            return 0
        length = 0
        prevVertex = path[0]
        for i in range(1, len(path)):
            curVertex = path[i]
            if graph[prevVertex].get(curVertex) is not None:
                length += graph[prevVertex][curVertex]
            prevVertex = curVertex
        return length
