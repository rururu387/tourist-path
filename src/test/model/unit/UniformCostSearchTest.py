import unittest

import pytest

from data.PathSearchTaskWithResult import PathSearchTaskWithResult
from initializer.TestManuallyDefinedGraphFactory import TestManuallyDefinedGraphFactory
from model.service.search.PathSearchService import PathSearchService
from model.service.search.dijkstra.UniformCostSearchService import UniformCostSearchService

uniformCostSearchService: PathSearchService = UniformCostSearchService()


@pytest.mark.parametrize("taskWithResult", TestManuallyDefinedGraphFactory.getManuallyCreatedPathSearchTasks())
def test_manuallyMarkedGraphs(taskWithResult: PathSearchTaskWithResult):
    actualPath = uniformCostSearchService.getPathByTask(taskWithResult)
    assert taskWithResult.shortestPath == actualPath
