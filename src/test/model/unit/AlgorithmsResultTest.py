# import unittest
#
# import numpy as np
#
#
# from initializer.TestRandomGraphFactory import TestRandomGraphFactory
# from service.PathAnalyserService import PathAnalyser
# from service.search.complete.ModifiedCompleteSearchService import ModifiedCompleteSearchService
#
#
# class AlgorithmsResultTest(unittest.TestCase):
#     def checkPathExistsInGraph(self, graph: list[dict[int, int]], path: list[int], startVertex: int, endVertex: int):
#         if len(path) == 0:
#             self.assertTrue(startVertex == endVertex)
#             return
#         self.assertFalse(len(path) == 1, "Path cannot contain only one vertex!")
#         currentVertex = path[0]
#         self.assertEqual(currentVertex, startVertex, "Path must start with startVertex")
#         self.assertEqual(path[len(path) - 1], endVertex, "Path must end with endVertex")
#         for i in range(1, len(path)):
#             nextVertex = path[i]
#             self.assertTrue(graph[currentVertex].get(nextVertex) is not None, "Graph must contain edge from vertex " +
#                             str(currentVertex) + " to vertex " + str(nextVertex) + "! Path is not valid for this graph")
#             currentVertex = nextVertex
#
#     def checkPathIsShorterThanThreshold(self, graph: list[dict[int, int]], path: list[int], lengthThreshold: int):
#         actualLength: int = PathAnalyser.getLength(graph, path)
#         self.assertLessEqual(actualLength, lengthThreshold)
#
#     def test_checkReturnedPathIsValid(self):
#         for i in range(0, 300):
#             completeSearchService = ModifiedCompleteSearchService()
#             vertexAmount = int(np.random.uniform(2, 20))
#
#             # Условия задачи
#             graph: list[dict[int, int]] = TestRandomGraphFactory.generateRandomGraph(vertexAmount)
#             lengthThreshold: int = int(np.random.uniform(3000, 5000))
#
#             # Разные алгоритмы решения
#             completeSearchPath = completeSearchService.getPath(graph, 0, vertexAmount, lengthThreshold)
#             uniformCostPath = UniformCostSearchService.getPath(graph, 0, vertexAmount, lengthThreshold)
#             modifiedUniformCostPath = ModifiedUniformCostSearchService.getPath(graph, 0, vertexAmount, lengthThreshold)
#             if uniformCostPath is None:
#                 self.assertTrue(completeSearchPath is None)
#                 self.assertTrue(modifiedUniformCostPath is None)
#             else:
#                 self.checkPathExistsInGraph(graph, completeSearchPath, 0, vertexAmount)
#                 self.checkPathIsShorterThanThreshold(graph, completeSearchPath, lengthThreshold)
#                 self.checkPathExistsInGraph(graph, uniformCostPath, 0, vertexAmount)
#                 self.checkPathIsShorterThanThreshold(graph, uniformCostPath, lengthThreshold)
#                 self.checkPathExistsInGraph(graph, modifiedUniformCostPath, 0, vertexAmount)
#                 self.checkPathIsShorterThanThreshold(graph, modifiedUniformCostPath, lengthThreshold)
#
#     if __name__ == '__main__':
#         unittest.main()
