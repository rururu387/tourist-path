import pytest

from data.PathSearchTaskWithResult import PathSearchTaskWithResult
from initializer.TestManuallyDefinedGraphFactory import TestManuallyDefinedGraphFactory
from model.service.search.PathSearchService import PathSearchService
from model.service.search.dijkstra.ModifiedUniformCostSearchService import ModifiedUniformCostSearchService


modifiedUniformCostSearchService: PathSearchService = ModifiedUniformCostSearchService()


@pytest.mark.parametrize("taskWithResult", TestManuallyDefinedGraphFactory.getManuallyCreatedPathSearchTasks())
def test_manuallyMarkedGraphs(taskWithResult: PathSearchTaskWithResult):
    actualPath = modifiedUniformCostSearchService.getPathByTask(taskWithResult)
    assert taskWithResult.modifiedUniformCostPath == actualPath