import pytest

from data.PathSearchTaskWithResult import PathSearchTaskWithResult
from initializer.TestManuallyDefinedGraphFactory import TestManuallyDefinedGraphFactory
from model.service.search.PathSearchService import PathSearchService
from model.service.search.complete.ModifiedCompleteSearchService import ModifiedCompleteSearchService
from model.service.search.dijkstra.ModifiedUniformCostSearchService import ModifiedUniformCostSearchService

modifiedCompleteSearchService: PathSearchService = ModifiedCompleteSearchService()


@pytest.mark.parametrize("taskWithResult", TestManuallyDefinedGraphFactory.getManuallyCreatedPathSearchTasks())
def test_manuallyMarkedGraphs(taskWithResult: PathSearchTaskWithResult):
    actualPath = modifiedCompleteSearchService.getPathByTask(taskWithResult)
    assert taskWithResult.completeSearchPath == actualPath

def test_1():
    graph = [{1: 1108, 2: 1956, 5: 1704}, {0: 721, 5: 652, 6: 1015, 9: 1954}, {0: 882, 5: 917, 7: 1817}, {}, {5: 1077, 8: 1622}, {2: 1304, 4: 1152}, {1: 1175, 2: 854}, {1: 1448, 2: 1983, 6: 399, 8: 521, 9: 164}, {3: 833, 4: 1688, 7: 772, 9: 1283}, {7: 179, 8: 1392}]
    mcss = ModifiedUniformCostSearchService()
    betterPath = mcss.getPath(graph, 0, 9, 3800)
    actualPath = modifiedCompleteSearchService.getPath(graph, 0, 9, 3800)