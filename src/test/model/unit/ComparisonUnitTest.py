import pytest

from model.data.PathSearchTask import PathSearchTask
from initializer.TestRandomGraphFactory import TestRandomGraphFactory
from model.service.PathAnalyserService import PathAnalyser
from model.service.search.PathSearchService import PathSearchService
from model.service.search.complete.ModifiedCompleteSearchService import ModifiedCompleteSearchService
from model.service.search.dijkstra.ModifiedUniformCostSearchService import ModifiedUniformCostSearchService
from model.service.search.dijkstra.UniformCostSearchService import UniformCostSearchService


def checkPathExistsInGraph(graph: list[dict[int, int]], path: list[int], startPoint: int, endPoint: int):
    if len(path) == 0:
        assert startPoint == endPoint
        return
    assert len(path) != 1, "Path cannot contain only one vertex!"
    currentVertex = path[0]
    for i in range(1, len(path)):
        nextVertex = path[i]
        assert graph[currentVertex].get(nextVertex) is not None, ("Graph must contain edge from vertex " +
                                                                  str(currentVertex) + " to vertex " + str(
                    nextVertex) + "! Path is not valid for this graph")
        currentVertex = nextVertex


def checkPathIsShorterThanThreshold(graph: list[dict[int, int]], path: list[int], lengthThreshold: int):
    actualLength: int = PathAnalyser.getLength(graph, path)
    assert actualLength <= lengthThreshold, "Path's length must be less than lengthThreshold"


def checkPathStartsAndEndsCorrectly(path: list[int], startPoint: int, endPoint: int):
    assert path[0] == startPoint, "Path must start with startVertex"
    assert path[len(path) - 1] == endPoint, "Path must end with endVertex"


def checkPath(pathSearchTask: PathSearchTask, path: list[int]):
    checkPathStartsAndEndsCorrectly(path, pathSearchTask.startPoint, pathSearchTask.endPoint)
    checkPathIsShorterThanThreshold(pathSearchTask.graph, path, pathSearchTask.timeSecondsThreshold)
    checkPathExistsInGraph(pathSearchTask.graph, path, pathSearchTask.startPoint, pathSearchTask.endPoint)


uniformCostSearchService: PathSearchService = UniformCostSearchService()
modifiedUniformCostSearchService: PathSearchService = ModifiedUniformCostSearchService()
modifiedCompleteSearchService: PathSearchService = ModifiedCompleteSearchService()


@pytest.mark.parametrize("pathSearchTask", TestRandomGraphFactory.getGeneratedPathSearchTasks(2, 100, 1000))
def test_modifiedUniformSearchOnAutoGeneratedGraphs(pathSearchTask: PathSearchTask):
    uniformCostSearchResultPath: list[int] = uniformCostSearchService.getPathByTask(pathSearchTask)
    modifiedUniformCostSearchResultPath: list[int] = modifiedUniformCostSearchService.getPathByTask(pathSearchTask)
    if uniformCostSearchResultPath is None:
        assert modifiedUniformCostSearchResultPath is None
    if uniformCostSearchResultPath is not None:
        checkPath(pathSearchTask, uniformCostSearchResultPath)
        checkPath(pathSearchTask, modifiedUniformCostSearchResultPath)

# Алгоритм не может завершиться для графа даже с 40 вершинами
@pytest.mark.parametrize("pathSearchTask", TestRandomGraphFactory.getGeneratedPathSearchTasks(20, 20, 1))
def test_completeSearchOnAutoGeneratedGraphs(pathSearchTask: PathSearchTask):
    uniformCostSearchResultPath: list[int] = uniformCostSearchService.getPathByTask(pathSearchTask)
    modifiedCompleteSearchServicePath: list[int] = modifiedCompleteSearchService.getPathByTask(pathSearchTask)
    if uniformCostSearchResultPath is None:
        assert modifiedCompleteSearchServicePath is None
    if uniformCostSearchResultPath is not None:
        print(pathSearchTask.graph)
        checkPath(pathSearchTask, uniformCostSearchResultPath)
        checkPath(pathSearchTask, modifiedCompleteSearchServicePath)


@pytest.mark.parametrize("pathSearchTask", TestRandomGraphFactory.getGeneratedPathSearchTasks(10, 20, 1000))
def test_autoGeneratedGraphs(pathSearchTask: PathSearchTask):
    uniformCostSearchResultPath: list[int] = uniformCostSearchService.getPathByTask(pathSearchTask)
    modifiedUniformCostSearchResultPath: list[int] = modifiedUniformCostSearchService.getPathByTask(pathSearchTask)
    modifiedCompleteSearchResultPath: list[int] = modifiedCompleteSearchService.getPathByTask(pathSearchTask)
    if uniformCostSearchResultPath is None:
        assert modifiedUniformCostSearchResultPath is None
        assert modifiedCompleteSearchResultPath is None
    if uniformCostSearchResultPath is not None:
        checkPath(pathSearchTask, uniformCostSearchResultPath)
        checkPath(pathSearchTask, modifiedUniformCostSearchResultPath)
        checkPath(pathSearchTask, modifiedCompleteSearchResultPath)
