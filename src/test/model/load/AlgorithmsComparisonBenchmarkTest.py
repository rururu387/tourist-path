import timeit

from data.AlgorithmRunResult import AlgorithmRunResult
from initializer.TestRandomGraphFactory import TestRandomGraphFactory
from model.service.search.PathSearchService import PathSearchService
from model.service.search.complete.ModifiedCompleteSearchService import ModifiedCompleteSearchService
from model.service.search.dijkstra.ModifiedUniformCostSearchService import ModifiedUniformCostSearchService
from model.service.search.dijkstra.UniformCostSearchService import UniformCostSearchService

uniformCostSearchService: PathSearchService = UniformCostSearchService()
modifiedUniformCostSearchService: PathSearchService = ModifiedUniformCostSearchService()
modifiedCompleteSearchService: PathSearchService = ModifiedCompleteSearchService()

# Uniform cost search results: avg time - 0.000044205, avg vertexes amount - 5.129928141, avg path length - 2696.516216741
# Modified uniform cost search results: avg time - 0.000095731, avg vertexes amount - 5.894542630, avg path length - 2921.051660517

def main():
    modifiedCompleteSearchResults: list[AlgorithmRunResult] = list()
    uniformCostSearchResults: list[AlgorithmRunResult] = list()
    modifiedUniformCostSearchResults: list[AlgorithmRunResult] = list()
    vertexAmount: int = 20
    calculationsAmount = 10000

    graphs = list()
    lengthThreshold: int = 3300
    for curCalculation in range(0, calculationsAmount):
        # if curCalculation % 10 == 0:
        print("Calculating " + str(curCalculation) + "th path")
        graph: list[dict[int, int]] = TestRandomGraphFactory.generateRandomGraph(vertexAmount)
        graphs.append(graph)
        # sqrt(1800^2*2) = 2545.58, 2545*1.5 ≈ 3800

        def runUniformCostSearch():
            path = uniformCostSearchService.getPath(graph, 0, vertexAmount - 1, lengthThreshold)
            algorithmResult = AlgorithmRunResult()
            algorithmResult.path = path
            uniformCostSearchResults.append(algorithmResult)

        uniformCostSearchTime: float = timeit.Timer(runUniformCostSearch).timeit(number=1)
        uniformCostSearchResults[curCalculation].executionSeconds = uniformCostSearchTime

        def runModifiedUniformCostSearch():
            path = modifiedUniformCostSearchService.getPath(graph, 0, vertexAmount - 1, lengthThreshold)
            algorithmResult = AlgorithmRunResult()
            algorithmResult.path = path
            modifiedUniformCostSearchResults.append(algorithmResult)

        modifiedUniformCostSearchTime: float = timeit.Timer(runModifiedUniformCostSearch).timeit(number=1)
        modifiedUniformCostSearchResults[curCalculation].executionSeconds = modifiedUniformCostSearchTime

        def runCompleteSearch():
            path = modifiedCompleteSearchService.getPath(graph, 0, vertexAmount - 1, lengthThreshold)
            algorithmResult = AlgorithmRunResult()
            algorithmResult.path = path
            modifiedCompleteSearchResults.append(algorithmResult)

        completeSearchTime: float = timeit.Timer(runCompleteSearch).timeit(number=1)
        modifiedCompleteSearchResults[curCalculation].executionSeconds = completeSearchTime

        if uniformCostSearchResults[curCalculation].path is None or \
            modifiedUniformCostSearchResults[curCalculation].path is None or \
            modifiedCompleteSearchResults[curCalculation].path is None:
            assert uniformCostSearchResults[curCalculation].path is None and \
                   modifiedUniformCostSearchResults[curCalculation].path is None and \
                    modifiedCompleteSearchResults[curCalculation].path is None
            continue

        assert uniformCostSearchResults[curCalculation].getPathLength(graph) <= \
                                                modifiedUniformCostSearchResults[curCalculation].getPathLength(graph)
        assert uniformCostSearchResults[curCalculation].getPathLength(graph) <= \
                                                modifiedCompleteSearchResults[curCalculation].getPathLength(graph)

        assert uniformCostSearchResults[curCalculation].getAmountOfDistinctVertexesInPath() <= \
                                    modifiedCompleteSearchResults[curCalculation].getAmountOfDistinctVertexesInPath()
        assert modifiedCompleteSearchResults[curCalculation].getAmountOfDistinctVertexesInPath() <= \
                                    modifiedCompleteSearchResults[curCalculation].getAmountOfDistinctVertexesInPath()

    (uniformCostSearchAvgTime, uniformCostAvgDistinctVertexes, uniformCostSearchAvgPathLength) = \
                                                    AlgorithmRunResult.getAvgParams(graphs, uniformCostSearchResults)
    (modifiedUniformCostSearchAvgTime, modifiedUniformCostSearchAvgDistinctVertexes,
                                                                            modifiedUniformCostSearchAvgPathLength) = \
                                            AlgorithmRunResult.getAvgParams(graphs, modifiedUniformCostSearchResults)
    (completeSearchAvgTime, completeSearchAvgDistinctVertexes, completeSearchAvgPathLength) = \
                                                AlgorithmRunResult.getAvgParams(graphs, modifiedCompleteSearchResults)

    print("Uniform cost search results: avg time - {:.9f}, avg vertexes amount - {:.9f}, avg path length - {:.9f}"\
                .format(uniformCostSearchAvgTime, uniformCostAvgDistinctVertexes, uniformCostSearchAvgPathLength))
    print("Modified uniform cost search results: avg time - {:.9f}, avg vertexes amount - {:.9f}, avg path length - "
          "{:.9f}".format(modifiedUniformCostSearchAvgTime, modifiedUniformCostSearchAvgDistinctVertexes,
                                                                                modifiedUniformCostSearchAvgPathLength))
    print("Complete search results: avg time - {: .9f}, avg vertexes amount - {:.9f}, avg path length - {:.9f}"
                .format(completeSearchAvgTime, completeSearchAvgDistinctVertexes, completeSearchAvgPathLength))



if __name__ == "__main__":
    main()
