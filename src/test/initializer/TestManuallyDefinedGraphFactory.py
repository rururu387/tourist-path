from data.PathSearchTaskWithResult import PathSearchTaskWithResult


class TestManuallyDefinedGraphFactory:
    @staticmethod
    def getGraph1() -> list[dict[int, int]]:
        graph = list()
        graph.append(dict({1: 7, 2: 3, 5: 23}))
        graph.append(dict({4: 11}))
        graph.append(dict({1: 5, 3: 17}))
        graph.append(dict({1: 15, 4: 34}))
        graph.append(dict({5: 5}))
        graph.append(dict())
        return graph

    @staticmethod
    def getGraph2() -> list[dict[int, int]]:
        graph = list()
        graph.append(dict({1: 7, 3: 5}))
        graph.append(dict({2: 1, 4: 11}))
        graph.append(dict({1: 1}))
        graph.append(dict({1: 3}))
        graph.append(dict())
        return graph

    @staticmethod
    def getGraph3() -> list[dict[int, int]]:
        graph = list()
        graph.append(dict({1: 15}))
        graph.append(dict({2: 1}))
        graph.append(dict({3: 1, 4: 20}))
        graph.append(dict({1: 1}))
        graph.append(dict())
        return graph

    @staticmethod
    def getGraph4() -> list[dict[int, int]]:
        graph = list()
        graph.append(dict({1: 1}))
        graph.append(dict({2: 1, 4: 1}))
        graph.append(dict({3: 2}))
        graph.append(dict())
        graph.append(dict({3: 4}))
        graph.append(dict({6: 4}))
        graph.append(dict({7: 1}))
        graph.append(dict())
        return graph

    @staticmethod
    def getGraph5() -> list[dict[int, int]]:
        return [{1: 2009, 3: 3419, 4: 1345}, {0: 2083, 2: 3562, 4: 71}, {1: 2918, 3: 3372, 4: 2285}, {0: 3546, 2: 3399, 4: 197}, {0: 2798, 2: 2705, 3: 218}]

    @staticmethod
    def getManuallyCreatedPathSearchTasks() -> list[PathSearchTaskWithResult]:
        pathSearchTasksWithExpectedResults: list[PathSearchTaskWithResult] = list()

        graph1NoPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph1(),
                                                                              0, 5, 22, None, None, None)
        pathSearchTasksWithExpectedResults.append(graph1NoPathTask)

        graph1ShortestPath: list[int] = [0, 1, 4, 5]
        graph1ShortestPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph1(),
                                                                                    0, 5, 23, graph1ShortestPath, graph1ShortestPath, graph1ShortestPath)
        pathSearchTasksWithExpectedResults.append(graph1ShortestPathTask)

        graph1Threshold24Path: list[int] = [0, 2, 1, 4, 5]
        graph1Threshold24PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph1(),
                                                                                       0, 5, 24, graph1ShortestPath, graph1Threshold24Path, graph1Threshold24Path)
        pathSearchTasksWithExpectedResults.append(graph1Threshold24PathTask)

        graph1Threshold51Path: list[int] = [0, 2, 3, 1, 4, 5]
        graph1Threshold51PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph1(),
                                                                                       0, 5, 51, graph1ShortestPath, graph1Threshold51Path, graph1Threshold51Path)
        pathSearchTasksWithExpectedResults.append(graph1Threshold51PathTask)

        graph1Threshold1000PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph1(),
                                                                                         0, 5, 1000, graph1ShortestPath, graph1Threshold51Path, graph1Threshold51Path)
        pathSearchTasksWithExpectedResults.append(graph1Threshold1000PathTask)

        graph2NoPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph2(),
                                                                              0, 5, 17, None, None, None)
        pathSearchTasksWithExpectedResults.append(graph2NoPathTask)

        graph2ShortestPath: list[int] = [0, 1, 4]
        graph2ShortestPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph2(),
                                                                                    0, 4, 18, graph2ShortestPath, graph2ShortestPath, graph2ShortestPath)
        pathSearchTasksWithExpectedResults.append(graph2ShortestPathTask)

        graph2Threshold19Path: list[int] = [0, 3, 1, 4]
        graph2Threshold19PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph2(),
                                                                                       0, 4, 19, graph2ShortestPath, graph2Threshold19Path, graph2Threshold19Path)
        pathSearchTasksWithExpectedResults.append(graph2Threshold19PathTask)

        graph2Threshold20PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph2(),
                                                                                       0, 4, 20, graph2ShortestPath, graph2Threshold19Path, graph2Threshold19Path)
        pathSearchTasksWithExpectedResults.append(graph2Threshold20PathTask)

        graph2Threshold21Path: list[int] = [0, 3, 1, 2, 1, 4]
        graph2Threshold21PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph2(),
                                                                                       0, 4, 21, graph2ShortestPath, graph2Threshold21Path, graph2Threshold21Path)
        pathSearchTasksWithExpectedResults.append(graph2Threshold21PathTask)

        graph2Threshold30PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph2(),
                                                                                       0, 4, 30, graph2ShortestPath, graph2Threshold21Path, graph2Threshold21Path)
        pathSearchTasksWithExpectedResults.append(graph2Threshold30PathTask)

        graph3NoPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph3(),
                                                                              0, 5, 35, None, None, None)
        pathSearchTasksWithExpectedResults.append(graph3NoPathTask)

        graph3ShortestPath: list[int] = [0, 1, 2, 4]
        graph3ShortestPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph3(),
                                                                                    0, 4, 36, graph3ShortestPath, graph3ShortestPath, graph3ShortestPath)
        pathSearchTasksWithExpectedResults.append(graph3ShortestPathTask)

        graph3Threshold38Path: list[int] = [0, 1, 2, 3, 1, 2, 4]
        graph3Threshold38PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph3(),
                                                                                       0, 4, 39, graph3ShortestPath, graph3Threshold38Path, graph3Threshold38Path)
        pathSearchTasksWithExpectedResults.append(graph3Threshold38PathTask)

        graph3Threshold60PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph3(),
                                                                                       0, 4, 60, graph3ShortestPath, graph3Threshold38Path, graph3Threshold38Path)
        pathSearchTasksWithExpectedResults.append(graph3Threshold60PathTask)

        graph4NoPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph4(),
                                                                              0, 5, 35, None, None, None)
        pathSearchTasksWithExpectedResults.append(graph4NoPathTask)

        graph4ShortestPath: list[int] = [0, 1, 2, 3]
        graph4ShortestPathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph4(),
                                                                                    0, 3, 4, graph4ShortestPath, graph4ShortestPath, graph4ShortestPath)
        pathSearchTasksWithExpectedResults.append(graph4ShortestPathTask)

        graph4Threshold6PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph4(),
                                                                                      0, 3, 6, graph4ShortestPath, graph4ShortestPath, graph4ShortestPath)
        pathSearchTasksWithExpectedResults.append(graph4Threshold6PathTask)

        graph4Threshold20PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph4(),
                                                                                       0, 3, 20, graph4ShortestPath, graph4ShortestPath, graph4ShortestPath)
        pathSearchTasksWithExpectedResults.append(graph4Threshold20PathTask)

        graph4ShortestPathOtherPart: list[int] = [5, 6, 7]
        graph4OtherPartThreshold5PathTask: PathSearchTaskWithResult = PathSearchTaskWithResult(TestManuallyDefinedGraphFactory.getGraph4(),
                                                                                               5, 7, 5, graph4ShortestPathOtherPart, graph4ShortestPathOtherPart,
                                                                                               graph4ShortestPathOtherPart)
        pathSearchTasksWithExpectedResults.append(graph4OtherPartThreshold5PathTask)
        return pathSearchTasksWithExpectedResults