import numpy as np
import heapq

from model.data.PathSearchTask import PathSearchTask
from model.data.Vertex import Vertex


class TestRandomGraphFactory:
    minTrafficCoefficient: float = 0.5
    maxTrafficCoefficient: float = 1.5
    @staticmethod
    def generateVertexes(vertexAmount: int, xMin: int, xMax: int, yMin: int, yMax: int) -> list[Vertex]:
        vertexList = list()
        startVertex = Vertex(100, 100, 0)
        vertexList.append(startVertex)
        for i in range(1, vertexAmount - 1):
            vertex = Vertex(int(np.random.uniform(xMin, xMax)), int(np.random.uniform(yMin, yMax)), i)
            vertexList.append(vertex)
        endVertex = Vertex(1900, 1900, vertexAmount - 1)
        vertexList.append(endVertex)
        return vertexList

    @staticmethod
    def getNeighbourVertexes(vertexList: list[Vertex], idx: int) -> list[Vertex]:
        closestVertexes = []
        for i in range(0, len(vertexList)):
            if i == idx:
                continue
            distance: float = vertexList[idx].getDistance(vertexList[i])
            neighbourTuple = (-distance, vertexList[i])
            if len(closestVertexes) > 5:
                heapq.heappushpop(closestVertexes, neighbourTuple)
            else:
                heapq.heappush(closestVertexes, neighbourTuple)
        return closestVertexes

    @staticmethod
    def getPathRandomLength(v1: Vertex, v2: Vertex) -> int:
        pathLength: int = int(v1.getDistance(v2) * np.random.uniform(TestRandomGraphFactory.minTrafficCoefficient, TestRandomGraphFactory.maxTrafficCoefficient))
        if pathLength == 0:
            pathLength = 1
        return pathLength

    @staticmethod
    def addPath(graph: list[dict], vertexList: list[Vertex], v1: int, v2: int):
        sourceVertex = vertexList[v1]
        destVertex = vertexList[v2]
        graph[v1][v2] = TestRandomGraphFactory.getPathRandomLength(sourceVertex, destVertex)
        if np.random.uniform(0, 100) > 50:
            graph[v2][v1] = int(TestRandomGraphFactory.getPathRandomLength(destVertex, sourceVertex))
            if graph[v2][v1] == 0:
                graph[v2][v1] = 1

    @staticmethod
    def generateEdges(vertexList: list[Vertex]) -> list[dict[int, int]]:
        graph = list()
        for i in range(0, len(vertexList)):
            graph.append(dict())

        for i in range(0, len(vertexList)):
            neighbourVertexes = TestRandomGraphFactory.getNeighbourVertexes(vertexList, i)
            curVertex = vertexList[i]
            (distance, sourceVertex) = neighbourVertexes[TestRandomGraphFactory.getRandomNeighbourVertex(len(neighbourVertexes) - 1)]
            TestRandomGraphFactory.addPath(graph, vertexList, sourceVertex.num, i)

            for j in range(0, int(np.random.uniform(0, 5))):
                TestRandomGraphFactory.addPath(graph, vertexList, curVertex.num,
                                             neighbourVertexes[TestRandomGraphFactory.getRandomNeighbourVertex(len(neighbourVertexes) - 1)][1].num)

            for j in range(0, TestRandomGraphFactory.getAmountOfDistantVertexes()):
                randomOtherVertex = vertexList[TestRandomGraphFactory.getRandomOtherVertex(len(vertexList), i)]
                if randomOtherVertex != i:
                    TestRandomGraphFactory.addPath(graph, vertexList, i, randomOtherVertex.num)
        return graph

    @staticmethod
    def getRandomOtherVertex(vertexAmount: int, currentVertexIdx: int) -> int:
        otherVertex = currentVertexIdx
        while otherVertex == currentVertexIdx:
            otherVertex = int(np.random.uniform(0, vertexAmount))
        return otherVertex

    @staticmethod
    def getRandomNeighbourVertex(maxNum: int = 5) -> int:
        probabilityArr = [60, 74, 82, 91, 95, 100]
        randNum = int(np.random.uniform(0, probabilityArr[maxNum]))
        if randNum <= probabilityArr[0]:
            return 0
        if randNum <= probabilityArr[1]:
            return 1
        if randNum <= probabilityArr[2]:
            return 2
        if randNum <= probabilityArr[3]:
            return 3
        if randNum <= probabilityArr[4]:
            return 4
        return 5

    @staticmethod
    def getAmountOfDistantVertexes() -> int:
        probabilityArr = [950, 980, 996]
        randNum = np.random.uniform(0, 1000)
        if randNum <= probabilityArr[0]:
            return 0
        if randNum <= probabilityArr[1]:
            return 1
        if randNum <= probabilityArr[2]:
            return 2
        return 3

    @staticmethod
    def generateRandomGraph(vertexAmount: int) -> list[dict[int, int]]:
        vertexes: list[Vertex] = TestRandomGraphFactory.generateVertexes(vertexAmount, 0, 2000, 0, 2000)
        return TestRandomGraphFactory.generateEdges(vertexes)

    @staticmethod
    def getGeneratedPathSearchTasks(minVertexes: int, maxVertexes: int, taskAmount: int) -> list[PathSearchTask]:
        pathSearchTasks: list[PathSearchTask] = list()
        for i in range(0, taskAmount):
            curGraphVertexAmount = int(np.random.uniform(minVertexes, maxVertexes + 1))
            curGraph: list[dict[int, int]] = TestRandomGraphFactory.generateRandomGraph(curGraphVertexAmount)
            pathSearchTask: PathSearchTask = PathSearchTask(curGraph, 0, curGraphVertexAmount - 1, int(np.random.uniform(3000, 4500)))
            pathSearchTasks.append(pathSearchTask)
        return pathSearchTasks
