from model.data.PathSearchTask import PathSearchTask


class PathSearchTaskWithResult(PathSearchTask):
    shortestPath: list[int] | None
    completeSearchPath: list[int] | None
    modifiedUniformCostPath: list[int] | None

    def __init__(self, graph: list[dict[int, int]], startPoint: int, endPoint: int, timeSecondsThreshold: int,
                 shortestPath: list[int] | None, completeSearchPath: list[int] | None, modifiedUniformCostPath: list[int] | None):
        super().__init__(graph, startPoint, endPoint, timeSecondsThreshold)
        self.shortestPath = shortestPath
        self.completeSearchPath = completeSearchPath
        self.modifiedUniformCostPath = modifiedUniformCostPath
