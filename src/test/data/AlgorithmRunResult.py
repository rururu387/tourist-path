import numpy as np

from model.service.PathAnalyserService import PathAnalyser


class AlgorithmRunResult:
    path: list[int]
    executionSeconds: float

    # def __init__(self, executionSeconds: float, path: list[int]):
    #     self.executionSeconds = executionSeconds
    #     self.amountOfDistinctVertexesInPath = np.unique(path).size

    def getAmountOfDistinctVertexesInPath(self):
        return np.unique(self.path).size

    def getPathLength(self, graph: list[dict[int, int]]):
        return PathAnalyser.getLength(graph, self.path)

    @staticmethod
    def getAvgParams(graphs: list[list[dict[int, int]]], results):
        avgExecutionSeconds: float = 0
        avgPathLength: float = 0
        avgDistinctVertexes: float = 0
        pathsFound: int = 0
        for curRunIdx in range(0, len(results)):
            avgExecutionSeconds += results[curRunIdx].executionSeconds
            if results[curRunIdx].path is not None:
                pathsFound += 1
                avgPathLength += PathAnalyser.getLength(graphs[curRunIdx], results[curRunIdx].path)
                avgDistinctVertexes += np.unique(results[curRunIdx].path).size
        avgExecutionSeconds /= len(results)
        if pathsFound == 0:
            avgPathLength = -1
            avgDistinctVertexes = -1
        else:
            avgPathLength /= pathsFound
            avgDistinctVertexes /= pathsFound
        return (avgExecutionSeconds, avgDistinctVertexes, avgPathLength)
